let h1 = document.querySelector("h1");
let i = 0;
setInterval(() => {
	h1.style.color =
		"#" +
		Math.floor(Math.random() * 256 ** 3)
			.toString(16)
			.padStart(6, "0");
	h1.style.fontSize = (Math.floor(i / 5) ? i % 5 : 5 - (i % 5)) * 200 + "%";
	i++;
	i %= 10;
}, 100);
