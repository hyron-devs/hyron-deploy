#!/usr/bin/env node
const fs = require("fs");
const path = require("path");
const axios = require("axios");

const args = require("minimist")(process.argv.slice(2));

function printHelp() {
	console.log(`Usage: ${process.argv[1]} -h
       ${process.argv[1]} DIRECTORY [OPTIONS]

Options:
    -h, --help          Display this help page
    -m, --mode MODE     Deploy to bingus-files or floppa-files (default: floppa-files)
    -r, --remote URL    Remote to deploy to (default: https://files.colon3.lol)
    -v, --verbose       Be verbose
`);
}

if (args.h ?? args.help) {
	printHelp();
	process.exit();
}

const inputFolder = args._[0];
const outputUrl = args.r ?? args.remote ?? "https://files.colon3.lol";
const mode = args.m ?? args.mode ?? "floppa-files";
const verbose = args.v ?? args.verbose;

if (!inputFolder) {
	console.error("Error: You need to specify a directory");
	process.exit(1);
}
if (!["bingus-files", "floppa-files"].includes(mode)) {
	console.error("Error: Invalid mode: " + mode);
	console.error("Valid modes are 'bingus-files' and 'floppa-files'");
	process.exit(1);
}

const fsCache = {};

// Define a function to recursively read a directory and return an array of file paths
function getFiles(dir, fileList = []) {
	const files = fs.readdirSync(dir);
	files.forEach((file) => {
		const filePath = path.join(dir, file);
		if (fs.statSync(filePath).isDirectory()) {
			fileList = getFiles(filePath, fileList);
		} else {
			fileList.push(filePath);
		}
	});
	return fileList;
}

// Define a function to extract dependencies from an HTML file
function getDependencies(filePath) {
	const content = fsCache[filePath];
	const links = content.match(/href="(.*?)"/g) || [];
	const scripts = content.match(/src="(.*?)"/g) || [];
	return [...links, ...scripts]
		.map((link) => link.match(/"(.*?)"/)[1])
		.filter((link) => !/^http(s)?:\/\//.test(link))
		.filter((link) => !link.startsWith("#"))
		.map((link) => (link.endsWith("/") ? link + "index.html" : link))
		.map((link) => {
			if (link.startsWith("/")) return link.slice(1);
			else
				return getRelativePath(
					path.resolve(path.dirname(filePath), link)
				);
		});
}

function getRelativePath(file) {
	return path.relative(inputFolder, file);
}

// Define a function to build a dependency tree from the files in the input folder
function buildDependencyTree(files) {
	const tree = {};

	// Create a node in the tree for each file
	files.forEach((file) => {
		const fileName = getRelativePath(file);
		tree[fileName] = { file: file, dependencies: [], dependents: [] };
	});

	// Add dependencies and dependents to each node
	files.forEach((file) => {
		const fileName = getRelativePath(file);
		const dependencies = getDependencies(file);
		dependencies.forEach((dependency) => {
			if (tree[dependency]) {
				tree[fileName].dependencies.push(dependency);
				tree[dependency].dependents.push(fileName);
			}
		});
	});

	return tree;
}

// Define a function to detect circular dependencies in the dependency tree
function detectCircularDependencies(tree) {
	const nodes = Object.keys(tree);
	const visited = new Set();

	function hasCycle(node, path = new Set()) {
		if (visited.has(node)) {
			return false;
		}

		if (path.has(node)) {
			return true;
		}

		path.add(node);

		const dependencies = tree[node].dependencies;
		for (let i = 0; i < dependencies.length; i++) {
			if (hasCycle(dependencies[i], path)) {
				return true;
			}
		}

		path.delete(node);
		visited.add(node);

		return false;
	}

	for (let i = 0; i < nodes.length; i++) {
		const node = nodes[i];
		if (hasCycle(node)) {
			return true;
		}
	}

	return false;
}

// Define a function to upload a file to the HTTP service and return its URL
async function uploadFile(file) {
	console.log(`Uploading '${file}'`);
	if (mode === "bingus-files") {
		const formData = new FormData();
		formData.append(
			"file",
			new Blob([fsCache[file] ?? fs.readFileSync(file, "binary")]),
			path.basename(file)
		);

		const response = await axios.post(`${outputUrl}/up`, formData);
		return `${outputUrl}/${response.data}`;
	} else if (mode === "floppa-files") {
		const response = await axios.put(
			`${outputUrl}/${path.basename(file)}`,
			fsCache[file] ?? fs.readFileSync(file, "binary")
		);
		return `${outputUrl}/${response.data}`;
	} else {
		throw new Error("Invalid mode: " + mode);
	}
}

// Define a function to bundle files with no circular dependencies
async function bundleFiles() {
	if (verbose) console.log("Reading directory");
	const files = getFiles(inputFolder);
	if (verbose) console.log("Loading files to RAM");
	for (let file of files) fsCache[file] = fs.readFileSync(file, "binary");
	if (verbose) console.log("Building dependency tree");
	const tree = buildDependencyTree(files);

	if (verbose) console.log("Checking for circular dependencies");
	if (detectCircularDependencies(tree)) {
		throw new Error("Circular dependency detected");
	}

	const bundle = [];

	// Upload files in a topological order (no dependencies before dependents)
	if (verbose) console.log("Uploading");
	while (Object.keys(tree).length > 0) {
		const nodesWithNoDependencies = Object.values(tree)
			.filter(
				(node) =>
					node.dependencies.every(
						(dep) =>
							bundle.some((b) => b.file === dep) ||
							dep.startsWith(outputUrl)
					) && !bundle.some((b) => b.file === node.file)
			)
			.map((node) => node.file);
		if (nodesWithNoDependencies.length === 0) {
			throw new Error("Circular dependency detected");
		}

		for (let i = 0; i < nodesWithNoDependencies.length; i++) {
			const file = nodesWithNoDependencies[i];
			const url = await uploadFile(file);

			// Update URLs in dependents
			tree[getRelativePath(file)].dependents.forEach((dependent) => {
				const dependentPath = path.relative(
					".",
					path.resolve(inputFolder, dependent)
				);
				let content = fsCache[dependentPath].toString();
				content
					.match(new RegExp("/" + getRelativePath(file), "g"))
					?.forEach((match) => {
						content = content.replace(match, url);
					});
				content
					.match(
						new RegExp(
							path.relative(
								path.dirname(dependent),
								getRelativePath(file)
							),
							"g"
						)
					)
					?.forEach((match) => {
						content = content.replace(match, url);
					});
				fsCache[dependentPath] = Buffer.from(content);
				const dependentNode = tree[dependent];
				const index = dependentNode.dependencies.indexOf(
					getRelativePath(file)
				);
				dependentNode.dependencies[index] = url;
			});

			// Remove file from the tree and add to the bundle
			delete tree[getRelativePath(file)];
			bundle.push({ file, url });
		}
	}
	if (verbose) console.log("Upload complete");

	// Find and return the URLs of the entry HTML pages
	const entryURLs = bundle
		.filter(({ file }) => file.match(/\.x?html?$/))
		.filter(
			({ file }) =>
				!Object.values(tree).some((node) =>
					node.dependencies.includes(file)
				)
		);

	return { entryURLs, bundle };
}

// Call the function to bundle files
bundleFiles()
	.then(({ entryURLs: urls, bundle }) => {
		if (verbose) console.log(bundle);
		console.log(urls.map(({ file, url }) => `${file}: ${url}`).join("\n"));
	})
	.catch((error) => {
		console.error(error);
	});
